import 'package:eznior/data/entity/product.dart';

class ProductRepository {
  final List<Product> items = [
    Product("Gitar", "assets/images/poster3.jpg", 150, 99, 50),
    Product("Biola", "assets/images/poster2.jpg", 200, 99, 50),
    Product("Suling Bambu", "assets/images/poster4.jpg", 100, 45, 65),
    Product("Piano", "assets/images/poster1.jpg", 150, 79, 50),
    Product("Kecapi", "assets/images/poster5.jpg", 150, 79, 50),
  ];

  List<Product> getProducts() {
    return items;
  }
}
