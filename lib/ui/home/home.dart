import 'package:eznior/data/entity/product.dart';
import 'package:eznior/data/entity/shopping_type.dart';
import 'package:eznior/data/repository/product_repository.dart';
import 'package:eznior/data/repository/shopping_type_repository.dart';
import 'package:eznior/internal/constants.dart';
import 'package:eznior/ui/auth/Login.dart';
import 'package:eznior/ui/auth/registrasi.dart';
import 'package:eznior/ui/home/about.dart';
import 'package:eznior/ui/home/horizontal_list.dart';
import 'package:eznior/ui/home/image_slider.dart';
import 'package:eznior/ui/home/inputdata.dart';
import 'package:eznior/ui/home/profile.dart';
import 'package:eznior/ui/home/shopping_type_tile.dart';
import 'package:eznior/ui/subList/product_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:eznior/ui/widgets/my_text_button.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<ShoppingType> shoppingTypes = new List();
  List<Product> products = new List();

  final List<String> sliderItems = [
    "assets/images/poster1.jpg",
    "assets/images/poster5.jpg"
  ];

  @override
  void initState() {
    super.initState();
    shoppingTypes = FakeShoppingTypeGenerator().generate();
    products = ProductRepository().getProducts();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the Drawer if there isn't enough vertical
        // space to fit everything.
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
            DrawerHeader(
              child: Icon(
                Icons.account_circle_outlined,
                color: Colors.grey,
                size: 130,
              ),
              decoration: BoxDecoration(
                color: Colors.black,
              ),
            ),
            MyTextButton(
              bgColor: Colors.white,
              textColor: Colors.black,
              buttonName: 'Home',
              onTap: () {
                Navigator.push(
                    context,
                    CupertinoPageRoute(
                      builder: (context) => HomePage(),
                    ));
              },
            ),
            MyTextButton(
              bgColor: Colors.white,
              textColor: Colors.black,
              buttonName: 'Profile',
              onTap: () {
                Navigator.push(
                    context,
                    CupertinoPageRoute(
                      builder: (context) => Profile(),
                    ));
              },
            ),
            MyTextButton(
              bgColor: Colors.white,
              textColor: Colors.black,
              buttonName: 'Tambah Barang',
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.push(
                    context,
                    CupertinoPageRoute(
                      builder: (context) => InputPage(),
                    ));
              },
            ),
            MyTextButton(
              bgColor: Colors.white,
              textColor: Colors.black,
              buttonName: 'About',
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.push(
                    context,
                    CupertinoPageRoute(
                      builder: (context) => About(),
                    ));
              },
            ),
            MyTextButton(
              bgColor: Colors.white,
              textColor: Colors.black,
              buttonName: 'Login',
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.push(
                  context,
                  CupertinoPageRoute(
                    builder: (context) => SignInPage(),
                  ),
                );
              },
            ),
            MyTextButton(
              bgColor: Colors.white,
              textColor: Colors.black,
              buttonName: 'Registrasi',
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.push(
                  context,
                  CupertinoPageRoute(
                    builder: (context) => RegisterPage(),
                  ),
                );
              },
            ),
          ],
        ),
      ),
      backgroundColor: Colors.white,
      appBar: _buildToolbar(),
      body: SingleChildScrollView(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            _buildSearch(),
            ImageSlider(items: sliderItems, onClick: (position, image) {}),
            SizedBox(height: 10),
            Container(
              height: 150,
              child: ListView.builder(
                itemCount: shoppingTypes.length,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ProductListPage(
                                  index, shoppingTypes[index].name)));
                    },
                    child: ShoppingTypeTile(
                      name: shoppingTypes[index].name,
                      image: shoppingTypes[index].image,
                    ),
                  );
                },
              ),
            ),
            SizedBox(height: 20),
            HorizontalProductList(
              title: "Hot On Sale",
              items: products,
            ),
            SizedBox(height: 20),
            HorizontalProductList(
              title: "Tradisional",
              items: products.reversed.toList(),
            ),
            SizedBox(height: 20),
            HorizontalProductList(
              title: "Modern",
              items: products,
            ),
            SizedBox(height: 20),
            HorizontalProductList(
              title: "Aksesoris",
              items: products.reversed.toList(),
            )
          ],
        ),
      ),
    );
  }

  Widget _buildSearch() {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 16),
      padding: EdgeInsets.symmetric(horizontal: 16),
      width: MediaQuery.of(context).size.width,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(6),
          border: Border.all(width: 1, color: Colors.grey[400])),
      child: Row(
        children: <Widget>[
          Expanded(
            child: TextField(
              maxLines: 1,
              cursorColor: Colors.black,
              style: TextStyle(fontSize: 14, color: Colors.black),
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: "What are you looking for?",
                  hintStyle: TextStyle(fontSize: 14, color: Colors.grey[400])),
            ),
          ),
          GestureDetector(
            onTap: () {},
            child: Icon(
              Icons.search,
              color: Colors.grey[400],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildToolbar() {
    return AppBar(
      backgroundColor: Colors.white,
      elevation: 0,
      title: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          // GestureDetector(
          //   onTap: () {},
          //   child: Icon(
          //     Icons.sort,
          //     color: Colors.black,
          //   ),
          // ),
          // SizedBox(width: 16),
          Text(
            appName,
            style: TextStyle(
                color: Colors.black, fontSize: 20, fontFamily: toolbarFont),
          )
        ],
      ),
      actions: <Widget>[
        Padding(
          padding: EdgeInsets.only(right: 16),
          child: GestureDetector(
            onTap: () {},
            child: Icon(
              Icons.shopping_basket,
              color: Colors.black,
            ),
          ),
        )
      ],
    );
  }
}
